<?php

//open
$new = [];
$x = 0;
$filename = "source.txt";
$handle = fopen($filename, "r");
$sourceData =  fread($handle, filesize($filename));

//clean
$pattern = "/(\[[0-9]+\] => )(Array[\s]+\([\S\s]+?\))/";
$sourceData = preg_replace($pattern, "$2,", $sourceData);


//build
$sourceData = explode("Array", $sourceData);
foreach ($sourceData as $value) {
	$sub = explode("\n", $value);
	if(! $value ) continue;

	foreach ($sub as $subvalue) {
		preg_match('/\[([a-z]+)\] => /', $subvalue, $key, PREG_OFFSET_CAPTURE);
		preg_match('/\[[a-z]+\] => ([\S\s]+)?/', $subvalue, $val, PREG_OFFSET_CAPTURE);	

		if(! $key[1][0] ) continue;
		$new[$x][$key[1][0]] =  $val[1][0];

	}
	$x++;
}




//write
$fp = fopen('file.csv', 'w');
fputcsv($fp, array_keys($new[1]));
foreach ($new as $fields) {
	print_r($fields);
    fputcsv($fp, $fields);
}
fclose($fp);

?>